# -*- coding: utf-8 -*-
import json
import logging

from pymodbus.payload import BinaryPayloadDecoder

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)


def get_register_mapping():
    # with open('Modbus TCP (1).json') as f:
    with open("Modbus TCP.json") as f:
        return json.load(f)


def get_value(client, slave, data_point):
    address = data_point["tcpRegAddress"]
    count = data_point["size"]
    logger.debug(f"data point: {json.dumps(data_point)}")
    res = client.read_holding_registers(address, count=count, unit=slave)
    logger.debug(res)
    logger.debug(f"Registers at address {address}: {res.registers}")

    byteorder = {"LSB": "<", "MSB": ">"}[data_point["byteOrder"]]
    wordorder = {"LSW": "<", "MSW": ">"}[data_point["wordOrder"]]
    decoder = BinaryPayloadDecoder.fromRegisters(
        res.registers, byteorder=byteorder, wordorder=wordorder
    )

    decoded = {
        "INT16": decoder.decode_16bit_int,
        "INT32": decoder.decode_32bit_int,
        "INT64": decoder.decode_64bit_int,
        "UINT16": decoder.decode_16bit_uint,
        "UINT32": decoder.decode_32bit_uint,
        "UINT64": decoder.decode_64bit_uint,
    }[data_point["coding"]]() * data_point["multiplier"]
    logger.debug(f"Decoded value: {decoded}")

    return {
        "measurement_id": data_point["name"],
        "measurement_description": data_point["name"],
        "unit": data_point["unit"],
        "value": float(decoded),
    }


measurements = {
    "PV Carport": [
        "Active Exported Energy Total",
        "Active Net Energy Total",
        "Reactive Imported Energy Total",
        "Reactive Exported Energy Total",
        "Reactive Net Energy Total",
        "Apparent Imported Energy Total",
        "Apparent Exported Energy Total",
        "Apparent Net Energy Total",
        #        "CO₂",
        #        "Currency",
        "Active Imported Energy L1",
        "Active Imported Energy L2",
        "Active Imported Energy L3",
        "Active Exported Energy L1",
        "Active Exported Energy L2",
        "Active Exported Energy L3",
        "Active Net Energy L1",
        "Active Net Energy L2",
        "Active Net Energy L3",
        "Reactive Imported Energy L1",
        "Reactive Imported Energy L2",
        "Reactive Imported Energy L3",
        "Reactive Exported Energy L1",
        "Reactive Exported Energy L2",
        "Reactive Exported Energy L3",
        "Reactive Net Energy L1",
        "Reactive Net Energy L2",
        "Reactive Net Energy L3",
        "Apparent Imported Energy L2",
        "Apparent Imported Energy L3",
        "Apparent Exported Energy L1",
        "Apparent Exported Energy L2",
        "Apparent Exported Energy L3",
        "Apparent Net Energy L1",
        "Apparent Net Energy L2",
        "Apparent Net Energy L3",
        "Voltage L2",
        "Voltage L3",
        "Voltage L1-2",
        "Voltage L2-3",
        "Voltage L3-1",
        "Current L1",
        "Current L2",
        "Current L3",
        "Active Imported Power Total",
        "Active Imported Power L1",
        "Active Imported Power L2",
        "Active Imported Power L3",
        "Reactive Imported Power Total",
        "Reactive Imported Power L1",
        "Reactive Imported Power L2",
        "Reactive Imported Power L3",
        "Apparent Imported Power Total",
        "Apparent Imported Power L1",
        "Apparent Imported Power L2",
        "Apparent Imported Power L3",
        "Frequency",
        "Phase Angle Power Total",
        "Power Factor Total",
        "Power Factor L1",
        "Power Factor L2",
        "Power Factor L3",
        #        "Current Quadrant Total",
        #        "Current Quadrant L1",
        #        "Current Quadrant L2",
        #        "Current Quadrant L3",
        #        "Binary Output 1",
        #        "Counter  1",
        #        "Counter  2",
        #        "Counter  3",
        #        "Counter  4",
        #        "Serial",
        #        "Firmware version",
        #        "Product version",
        #        "Product name",
        #        "Error flags",
        #        "Information flags",
        #        "Warning flags",
        #        "Alarm flags",
        #        "Power fail counter",
        #        "CO2 conversion factor",
        #        "Currency conversion factor",
        #        "Number of elements",
        "Active Imported Energy Total",
        "Apparent Imported Energy L1",
        "Voltage L1",
    ],
    "Kwh 1": [
        "Current L1",
        "Active Imported Power Total",
        "Frequency",
        #        "Power Factor Total",
        #        "Binary Output 1",
        #        "Counter  1",
        #        "Counter  2",
        #        "Counter  3",
        #        "Counter  4",
        #        "Serial",
        #        "Firmware version",
        #        "Product version",
        #        "Product name",
        #        "Error flags",
        #        "Information flags",
        #        "Warning flags",
        #        "Alarm flags",
        #        "Number of elements",
        "Active Imported Energy Total",
        "Voltage L1",
    ],
    "Kwh 2": [
        "Current L1",
        "Active Imported Power Total",
        "Frequency",
        #        "Power Factor Total",
        #        "Binary Output 1",
        #        "Counter  1",
        #        "Counter  2",
        #        "Counter  3",
        #        "Counter  4",
        #        "Serial",
        #        "Firmware version",
        #        "Product version",
        #        "Product name",
        #        "Error flags",
        #        "Information flags",
        #        "Warning flags",
        #        "Alarm flags",
        #        "Number of elements",
        "Active Imported Energy Total",
        "Voltage L1",
    ],
}
