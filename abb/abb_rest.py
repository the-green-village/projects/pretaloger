# -*- coding: utf-8 -*-
import requests


def get_abb_lists(abb_host, abb_token):
    # Get the list of devices.
    # The following response is expected:
    # [
    #   {
    #     "fingerPrint": 1,
    #     "primaryAddress": 1,
    #     "serialNumber": "256177",
    #     "manufacturer": "ABB",
    #     "configStatus": true,
    #     "status": "OK",
    #     "medium": "Electricity",
    #     "version": 768,
    #     "baudrate": 9600
    #   },
    #   {
    #     "fingerPrint": 2,
    #     "primaryAddress": 2,
    #     "serialNumber": "39164",
    #     "manufacturer": "ABB",
    #     "configStatus": true,
    #     "status": "OK",
    #     "medium": "Electricity",
    #     "version": 768,
    #     "baudrate": 9600
    #   },
    #   {
    #     "fingerPrint": 3,
    #     "primaryAddress": 3,
    #     "serialNumber": "39203",
    #     "manufacturer": "ABB",
    #     "configStatus": true,
    #     "status": "OK",
    #     "medium": "Electricity",
    #     "version": 768,
    #     "baudrate": 9600
    #   }
    # ]
    # r = requests.get(f"{abb_host}/api/meters/list",
    #                  headers={"Authorization": f"Longterm {abb_token}"},
    r = requests.get(
        "%s/api/meters/list" % (abb_host),
        headers={"Authorization": "Longterm %s" % (abb_token)},
        timeout=5,
    )
    print("ABB /api/meters/list response code: %s" % (r.status_code))
    devices = r.json()

    # Get the list of possible data points for each device.
    # The ABB REST API returns the data points in the following format:
    # {
    #   "unit": "V",
    #   "multiplier": 0.1,
    #   "coding": 4,
    #   "size": 2,
    #   "msb": 1,
    #   "msw": 0,
    #   "recordNumber": 219904,
    #   "valueTypeDescription": "Voltage  phase 1",
    #   "valueTypeInfo": {
    #     "direction": null,
    #     "type": "Voltage",
    #     "subType": null,
    #     "subTypeSuffix": null,
    #     "module": "Phase 1",
    #     "group": "Instantaneous",
    #     "tariff": null
    #   }
    # }
    device_datapoints = []
    for d in devices:
        # r = requests.get(f"{abb_host}/api/meters/{d['fingerPrint']}/dataPoints/list",
        #                  headers={"Authorization": f"Longterm {abb_token}"},
        r = requests.get(
            "%s/api/meters/%d/dataPoints/list" % (abb_host, d["fingerPrint"]),
            headers={"Authorization": "Longterm %s" % (abb_token)},
            timeout=5,
        )
        print(
            "ABB /api/meters/%s/dataPoints/list response code: %s"
            % (d["fingerPrint"], r.status_code)
        )
        device_datapoints.append(
            {"fingerPrint": d["fingerPrint"], "dataPoints": r.json()}
        )

    return devices, device_datapoints


# Selection of quantities to send to The Green Village data platform.
# The ABB REST API returns all possible measurements for each device.
# Only the ones for which valueTypeDescription is in the valueTypesDescriptions
# below will be forwarded.
device_measurements = [
    {
        "fingerPrint": 1,
        "device_id": "PV_carport",
        "valueTypeDescriptions": [
            "Energy active exported total",
            "Energy active net total",
            "Energy reactive imported total",
            "Energy reactive exported total",
            "Energy reactive net total",
            "Energy apparent imported total",
            "Energy apparent exported total",
            "Energy apparent net total",
            "Energy active imported phase 1",
            "Energy active imported phase 2",
            "Energy active imported phase 3",
            "Energy active exported phase 1",
            "Energy active exported phase 2",
            "Energy active exported phase 3",
            "Energy active net phase 1",
            "Energy active net phase 2",
            "Energy active net phase 3",
            "Energy reactive imported phase 1",
            "Energy reactive imported phase 2",
            "Energy reactive imported phase 3",
            "Energy reactive exported phase 1",
            "Energy reactive exported phase 2",
            "Energy reactive exported phase 3",
            "Energy reactive net phase 1",
            "Energy reactive net phase 2",
            "Energy reactive net phase 3",
            "Energy apparent imported phase 2",
            "Energy apparent imported phase 3",
            "Energy apparent exported phase 1",
            "Energy apparent exported phase 2",
            "Energy apparent exported phase 3",
            "Energy apparent net phase 1",
            "Energy apparent net phase 2",
            "Energy apparent net phase 3",
            "Voltage  phase 1",
            "Voltage  phase 2",
            "Voltage  phase 3",
            "Voltage  phase 1-2",
            "Voltage  phase 2-3",
            "Voltage  phase 3-1",
            "Current  phase 1",
            "Current  phase 2",
            "Current  phase 3",
            "Power active imported total",
            "Power active imported phase 1",
            "Power active imported phase 2",
            "Power active imported phase 3",
            "Power reactive imported total",
            "Power reactive imported phase 1",
            "Power reactive imported phase 2",
            "Power reactive imported phase 3",
            "Power apparent imported total",
            "Power apparent imported phase 1",
            "Power apparent imported phase 2",
            "Power apparent imported phase 3",
        ],
    },
    {
        "fingerPrint": 2,
        "device_id": "kWh_meter_1",
        "valueTypeDescriptions": [
            "Current  phase 1",
            "Power active imported total",
            "Energy active imported total",
            "Voltage  phase 1",
        ],
    },
    {
        "fingerPrint": 3,
        "device_id": "kWh_meter_2",
        "valueTypeDescriptions": [
            "Current  phase 1",
            "Power active imported total",
            "Energy active imported total",
            "Voltage  phase 1",
        ],
    },
]
