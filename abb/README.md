# ABB

The **ABB EQmatic Energy Analyzer** connects to energy meters over Modbus RTU and makes it possible to read data via Modbus TCP or REST API.

The documentation on the ABB EQmatic Energy Analyzer can be found in
[QAS_xyy1_PH_EN_V1-3_2CDC512090D0202.pdf](QAS_xyy1_PH_EN_V1-3_2CDC512090D0202.pdf)
or downloaded from
https://library.e.abb.com/public/727e3de25c814fbabfcc105f0959e08e/QAS_xyy1_PH_EN_V1-3_2CDC512090D0202.pdf?x-sign=d8vK6hV1zoSPLx8NYryzUTag+H3VLTTlT1aeYexa97LSCkf38eF/P3u8gawRw6C6

## Modbus

[Modbus TCP.json](Modbus TCP.json) shows the register mapping and is downloaded from http://145.94.45.20/app/management/data-sharing

`mbpoll` is a command line utility to communicate with Modbus slave.

https://github.com/epsilonrt/mbpoll

This is an example of reading the frequency (register address 516) from kWh meter with slave ID 2:

```bash
docker build -t mbpoll . -f Dockerfile_mbpoll
docker run -it mbpoll bash
```

```bash
mbpoll -p 502 145.94.45.20 -a 2 -r 516 -t 4 -0
mbpoll -p 502 145.94.45.20 -a 2 -r 512 -t 4 -0
```

The [modbus-test.py](modbus-test.py) python script can be used for testing.

## REST API

The documentation on the REST API can be found in
[QAS_3xx1_PH_EN_V1-0_RestAPI.pdf](QAS_3xx1_PH_EN_V1-0_RestAPI.pdf)
or downloaded from
https://library.e.abb.com/public/479c60bf0a4846e7b1e41d9ee25836d4/QAS_3xx1_PH_EN_V1-0_RestAPI.pdf

```bash
curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/list | jq .

curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/1/dataPoints/list | jq .
curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/1/values | jq .

curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/2/dataPoints/list | jq .
curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/2/values | jq .

curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/3/dataPoints/list | jq .
curl -s -H "Authorization: Longterm $TOKEN" 145.94.45.20/api/meters/3/values | jq .
```

## Producing to Kafka

Three python scripts for producing to Kafka are available in this repository:
- [modbus-producer.py](modbus-producer.py) sends data obtained via Modbus using a Kafka producer (preferred way, more performant),
- [modbus-rest.py](modbus-rest.py) sends data obtained via Modbus to the Kafka REST Proxy.
- [rest-rest.py](rest-rest.py) sends data obtained via the ABB REAT API to the Kafka REST Proxy.

### Run locally

```bash
python -m venv venv
. venv/bin/activate

pip install -U -r requirements.txt
python modbus-producer.py
```

### Run with Docker

```bash
export DOCKER_ID_USER="salekd"
docker login https://index.docker.io/v1/

docker build . -t pretaloger-abb
docker tag pretaloger-abb $DOCKER_ID_USER/pretaloger-abb:3.0.1
docker push $DOCKER_ID_USER/pretaloger-abb:3.0.1

# docker run -v $(pwd)/pretaloger.cfg:/pretaloger.cfg pretaloger-abb python modbus-producer.py
# docker run -v $(pwd)/pretaloger.cfg:/pretaloger.cfg pretaloger-abb python rest-producer.py
# docker run -v $(pwd)/pretaloger.cfg:/pretaloger.cfg pretaloger-abb python rest-rest.py
docker run -v $(pwd)/pretaloger.cfg:/pretaloger.cfg pretaloger-abb
```

### Deploy on Kubernetes

```bash
# Create ConfigMap with the configuration file.
kubectl delete configmap pretaloger-abb -n ccloud
kubectl create configmap pretaloger-abb -n ccloud \
  --from-file=pretaloger.cfg=pretaloger.cfg

# Deploy a pod.
kubectl delete deploy pretaloger-abb -n ccloud
kubectl apply -f pretaloger-abb-dep.yaml

kubectl logs deploy/pretaloger-abb -n ccloud
```
