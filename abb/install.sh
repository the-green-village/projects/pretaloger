#!/bin/bash
# mo pretaloger.cfg_template > pretaloger.cfg

kubectl delete configmap pretaloger-abb -n tud-gv
kubectl create configmap pretaloger-abb -n tud-gv \
  --from-file=pretaloger.cfg=pretaloger.cfg

kubectl delete deploy pretaloger-abb -n tud-gv
kubectl apply -f pretaloger-abb-dep.yaml
