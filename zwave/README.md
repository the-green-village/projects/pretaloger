# North-Q pulse meters (deprecated)

**North-Q Q-Power** is a pulse meter connected over a **Z-Wave** network.

https://www.northq.com/q-power

Documentation:
- [b8e9e7_888e63d13c064feeb51b0998d99feaf1.pdf](b8e9e7_888e63d13c064feeb51b0998d99feaf1.pdf)
https://docs.wixstatic.com/ugd/b8e9e7_888e63d13c064feeb51b0998d99feaf1.pdf
- [b8e9e7_a64eb5dfb4a44d08bcbdfe102566d750.pdf](b8e9e7_a64eb5dfb4a44d08bcbdfe102566d750.pdf)
https://docs.wixstatic.com/ugd/b8e9e7_a64eb5dfb4a44d08bcbdfe102566d750.pdf
- [b8e9e7_f16f475ecff64ea7aaf1058a4c94bab5.pdf](b8e9e7_f16f475ecff64ea7aaf1058a4c94bab5.pdf)
https://docs.wixstatic.com/ugd/b8e9e7_f16f475ecff64ea7aaf1058a4c94bab5.pdf
