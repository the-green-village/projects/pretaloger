# Prêt-à-Loger

There are two ways of reading data from the energy meters in Prêt-à-Loger:
- Communicating with an **ABB EQmatic Energy Analyzer** that exposes the energy meter data over Modbus TCP or REST API.
- Communicating with an IoT gateway that reads pulse meters over a Z-Wave network (deprecated).


Next to that a solaredge entity is read out, the code for this is present in the pal_solaredge branch
